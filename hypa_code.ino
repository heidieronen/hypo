#include <SPI.h>
#include <WiFi101.h>
#include <WiFiSSLClient.h>
#include <MQTT.h>
//#include <MQTTClient.h>

// WLAN 
char ssid[] = "xxxx"; //  your network SSID (name)
char pass[] = "xxxxxxx";    // your network password (use for WPA)

/*use this class if you connect using SSL  WiFiSSLClient net; */
WiFiClient net;
MQTTClient MqttClient;

const int chipSelectPinLum = 2;
const int chipSelectMIC = 1;

unsigned long lastSampleMillis = 0;
unsigned long previousWiFiBeginMillis = 0;
unsigned long lastWatsonMillis = 0;        // POIS jos lähetys Watsonille ei perustu kuluneeseen aikaan

//*** variables for Light sensor data 
int recu[2];                  // Array to store voice samples (2 bytes)
int lumiere;                  //
int i;                        // Mahdollisesti turha

//*** variables for voice sensor (MIC) data
const int numSamples = 100;
int array_elements = 0;       // variable used to check how many voice samples have been read to array, if less than max, then value of this variable is used to calculate soundLevel
int soundByte1 = 0;           // 8 bit data from mic board, MSBits 
int soundByte2 = 0;           // 8 bit data from mic board
int sound12bit = 0;           // 12 bit voice sample, soundByte1 & soundByte2 combined [ 0000 nnnn nnnn nnnn ]                   nnn.. = two's complement! ,  in MKR1000 board SAMD21 processor the int is 32 bit two's complement ????
int sound12bitA[numSamples];  // Array to store 12bit voice samples
int sound12bitAbs = 0;        // abs of 12bit voice samples - 1024. Subtraction removes value appearing in each sample due to DC-component in Mic analog output 
int sampleIndex = 0; 
int soundSum = 0;             // Sum of voice samples (amount indicated by numSamples var) in array
int soundLevel = 0;           // 12bit positive number formed by taking average of (numsaples amount) sound12bitAbs value stored in array 



/**********/
SPISettings settingsA(2000000, MSBFIRST, SPI_MODE0); //K�ytett�v� piiri sallii datasheetin mukaan 1-4 MHz l�hetystehon

void setup()
{
  Serial.begin(9600);       // initialization of serial communication
  delay(2000);              // Wait for wifi unit to power up
  WiFi.begin(ssid, pass);
  delay(5000);              // Wait for WiFi to connect
  Serial.println("Connected to WLAN");
  printWiFiStatus();

MqttClient.begin("k4v8zt.messaging.internetofthings.ibmcloud.com", 1883, net);  // Cut for testing without Watson
connect();                // calling subroutine connect
   /* client.begin("<Address Watson IOT>", 1883, net);     Address Watson IOT: <WatsonIOTOrganizationID>.messaging.internetofthings.ibmcloud.com */

 SPI.begin(); // initialization of SPI port
   // SPI.setDataMode(SPI_MODE0); // configuration of SPI communication in mode 0
   //SPI.setClockDivider(SPI_CLOCK_DIV16); // configuration of clock at 1MHz
 pinMode(chipSelectPinLum, OUTPUT);        // defining that this MKR1000 pin number is used as a output
 pinMode(chipSelectMIC, OUTPUT);           // defining that this MKR1000 pin number is used as a output

 for(int i = 0; i < numSamples; i++)       // Initializing the sound sample of array to zero.
 {
  sound12bitA[i] = 0;
 }
}

void loop() {
 MqttClient.loop();  // Cut for testing without Watson
 
  // opening and closing SPI communication for reading light sensor (1st) and then Pmod MIC3 sensor
  if(millis() - lastSampleMillis > 1)   //500 used earlier, but higher sample frequency was wanted 
  { 
    lastSampleMillis = millis();
    SPI.beginTransaction(SPISettings(14000000, MSBFIRST, SPI_MODE0));
    digitalWrite(chipSelectPinLum, LOW); // activation of light sensor card CS line 
    for (i=0;i<2;i=i+1)
      {
      recu[i] = SPI.transfer(0); // Acquisition of the light samples (2 bytes of data)
      }
    digitalWrite(chipSelectPinLum, HIGH); // deactivation  of light sensor CS line 
    
    // send in the value in two bytes via SPI:
    lumiere=(recu[0]<<3)|(recu[1]>>4); // Reconstitution of the 8-bit light variable
    Serial.print("Lumiere=");
    Serial.println(lumiere);
    //delay(200); 

    digitalWrite(chipSelectMIC, LOW); // Ativating CS line of MIC sensor card
    delayMicroseconds(20);            // to quarantee success of reading voice sample data
    soundByte1 = SPI.transfer(0x00);  // Acquisition of the noise sample (1st byte)
    delayMicroseconds(20);
    soundByte2 = SPI.transfer(0x00);  // Acquisition of the noise sample (2nd byte)
    delayMicroseconds(20);
    digitalWrite(chipSelectMIC, HIGH); // deactivating CS line of MIC sensor card
    SPI.endTransaction();

    Serial.print("soundByte1=");       // for testing purposes
    Serial.println(soundByte1);        // for testing purposes
    Serial.print("soundByte2=");       // for testing purposes
    Serial.println(soundByte2);        // for testing purposes
    
    soundByte1 = soundByte1<<8;               // shifting 8 bits left
    sound12bit = soundByte1|soundByte2;       // combining soundByte1 and soundByte2 by using OR 
    sound12bitAbs = abs(sound12bit - 1024);   // forming 12bit voice sample, where value (1024) representing DC-component of analog MIC output is subtracted & negative values turned positive
    if(sound12bitAbs > 3071)                  // prevents occasional incorrect big values (two bytes of ones - 1024) affecting soundLevel value. 30171 is max value with 12bit -1024
    {
      sound12bitAbs=3071;
    }
    soundSum = soundSum - sound12bitA[sampleIndex];   // subtract the oldest sample
    sound12bitA[sampleIndex] = sound12bitAbs;         // reading the latest sample in the array
    soundSum = soundSum + sound12bitA[sampleIndex];   // adding the latest sample from array to soundSum
    
    sampleIndex = sampleIndex + 1;            // increasing sampleIndex
    array_elements = array_elements + 1;      // increasing array_elements
    
    if(array_elements < numSamples)             // this if-else structure takes care that soundLevel value is correctly formed also in a case when number of samples added to aound12bitA array is less than numSamples value. 
    { 
     soundLevel = soundSum / array_elements;    // forming average value of the voice over period of ~1sek (500 samples roughly 2ms intervals)
    } 
    else  
    { 
     soundLevel = soundSum / numSamples;        // forming average value of the voice over period of ~1sek (500 samples roughly 2ms intervals)
     array_elements = 1000;                     // seeting variable value to make sure variable value do not grow bigger than int variable can handle and thus cause problems
    } 

    if(sampleIndex >= numSamples)             // setting sampleIndex to 0 if sample index equals numSamples (amount of elements in array) 
    {
      sampleIndex = 0;
    }     

    for (i=0;i<2;i=i+1)               // For testing purposes - printing to serial monitor
      {
      Serial.print("i");
      Serial.print(i);
      Serial.print("=");
      Serial.println(recu[i]);
      }
      Serial.print("sound12bit=");    // For testing purposes - printing to serial monitor
      Serial.println(sound12bit);
      Serial.print("soundlevel=");
      Serial.println(soundLevel);
  }
      
  if(sampleIndex == 0)   // send to Bluemix device (=mqtt publish) always when array (sound12bitA) is full of new samples 
  {                      // first used condition  if(millis() - lastWatsonMillis > 30000)
   Serial.println("Publishing to Watson...");
   if(!MqttClient.connected()) {    
     connect();                 // calling subroutine connect
     }                           
     lastWatsonMillis = millis();
     MqttClient.publish("iot-2/evt/Hypa_update/fmt/json", "{\"Light\": " + String(lumiere)+", \"Noise\": " +String(soundLevel)+"}");
  }                             //  MqttClient.publish("iot-2/evt/TemperatureTC1/fmt/json", "{\"Temperature sensors\":\"TC1 \",\"TempScaledDF48\":\"" + String(tempScaledF)+"\", \"TempStreightDF48\": \"" + String(temp14bit)+"\"}");  
     delay(1);
}                                  // end of loop

void connect() 
{
  Serial.print("checking WLAN...");
  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.print(".");       // printing a dot every half second
    if ( millis() - previousWiFiBeginMillis > 5000) // reconnecting
    {
      previousWiFiBeginMillis = millis();
      WiFi.begin(ssid, pass);
      delay(5000); // Wait for WiFi to connect
      Serial.println("Connected to WLAN");
      printWiFiStatus();
    }
    delay(500); 
  }
       /*Example:
        MqttClient.connect("d:iqwckl:arduino:oxigenarbpm","use-token-auth","90wT2?a*1WAMVJStb1")
        Documentation: 
        https://console.ng.bluemix.net/docs/services/IoT/iotplatform_task.html#iotplatform_task */
 
  Serial.print("\nconnecting Watson with MQTT....");
  while (!MqttClient.connect("d:k4v8zt:ValoAnturi:VA_1234","use-token-auth","SZOes4?z7T)JUNY7iE")) 
  {
    Serial.print(".");
    delay(15000);
  }
  Serial.println("\nconnected!");
}

void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}